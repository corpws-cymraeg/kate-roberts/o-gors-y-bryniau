<xml>
<chapter n="05">Newid Byd</chapter>

{65} Un o’r boreau hynny yn yr haf ydoedd, pan fydd edafedd y gwawn yn dew, neu’n hytrach yn deneu ar hyd y perthi, a sŵn traed a siarad chwarelwyr i’w glywed fel sŵn gwenyn yn y pellter agos. Safai Wiliam Gruffydd a’i bwysau ar ddôr fach yr ardd â’i olygon yn edrych ymhell, heb fod yn edrych i unman neilltuol. Yr oedd rhyw olwg hiraethus yn ei lygad; ond, o ran hynny, fel yna’r edrychai Wiliam Gruffydd bob amser. Teimlech ryw ias tosturi yn cerdded eich holl gorff bob tro y byddech yn agos iddo. Y bore hwn edrychai’n fwy hiraethus nag arfer, ac yn rhy synfyfyriol i gymryd sylw o’r chwarelwyr yn codi eu dwylo arno wrth basio.

Yr oedd tair blynedd bellach er pan roddodd oreu i’r chwarel a symud o Fryn y Fawnog i Fodlondeb, o’r tyddyn i’r tŷ moel. Ychydig cyn hynny aethai Guto ei fab i’r Merica, ac efe a ddarbwyllodd ei rieni i symud i’r tŷ moel a gadael y chwarel er mwyn gorffwys tipyn ar ddiwedd eu gyrfa. O’r mis cyntaf wedi iddo gyrraedd gofalodd Guto am anfon pedair punt adref bob mis, a deuent mor rheolaidd a dydd Llun pen mis yn y chwarel. Rhwng yr arian hynny a phensiwn yr hen bobl, gallai {66} Wiliam a Margiad ei wraig gael popeth yr oedd arnynt ei eisiau. Ond bu Margiad farw ymhen dwy flynedd ar ôl symud; a’i geiriau olaf hi oedd am wartheg a moch ac anifeiliaid felly. A chan na roddir “geiriau olaf” o’r natur yna ar gerdyn coffa neb, bu raid gadael ei cherdyn hi heb ei geiriau olaf. Wedyn disgwyliai Wiliam Gruffydd Guto adref o’r Merica. Syniad yr hen ŵr am y wlad tuhwnt i’r Werydd ydoedd, gwlad a digon o aur ynddi, dim ond i chwi fynd yno i’w hel, fel yr aech i Dwyn y Cyll i hel mwyar duon ym mis Medi. Iddo ef, dyna’r rheswm paham y deuai pobl oddiyno mor fuan gyda chotiau a phocedi mawr ac aur yn eu dannedd gosod. Synnai felly na ddeuai Guto’n ôl. Buasai’n well ganddo erbyn hyn weled Guto na chael yr arian bob mis.

Wedi marw Margiad cafodd eneth i gadw’i dŷ, ond ni welai hi na’r tŷ fawr arno o fore hyd nos. Treuliai ei amser a’i bwysau ar ddôr yr ardd neu’n siarad â dynion trwsio’r ffordd. Mewn gair, yr oedd ar goll oddiar y dydd y gadawodd y chwarel. Y mae’n wir bod iddo atgofion melys am Fryn y Fawnog. Ond, chwedl yntau, “Mi fasa ar ddyn hiraeth ar ôl Gehenna petai o wedi bod yno ddigon hir i gynefino yno.” Mewn gwirionedd, yr oedd yr {67} atgofion melys am y tyddyn yn eu cyfyngu eu hunain i foch yn marw o’r clwy, a gwartheg yn nychu o’r clwy bustl. Ond wedyn, edrych i Sodom a wnaiff dyn oni bydd ganddo rywle gwell i edrych iddo. A threuliodd Wiliam Gruffydd lawer awr â’i bwysau ar ddôr yr ardd yn edrych i gyfeiriad Bryn y Fawnog. Ac yn niffyg gwell gwaith beirniadai ei olynydd. Oedd, yr oedd gwartheg y tenant newydd yn deneuach na’i eiddo ef gynt. Yr oedd eu hesgyrn allan drwy eu crwyn bron. Ac wedyn, nid oedd fawr o gamp ar ei wair er yr holl basic slag a roddai’r tenant newydd i’w dir.

“’Dydw i ’n gweld gyno fo fawr gwell gwair,” ebe fo wrth John Jôs y ffordd ryw ddiwrnod, “efo’i holl fagic slag a’i ffansi tail. Mi ’rydw i ’n dallt hen dir Bryn y Fawnog ’na i’r dim. Cariwch chi ddigon o dail o’r doman iddo fo, a dawnsiwch dipyn hyd yr hen werglodd yna i gau hôl traed gwarthaig, a fydd raid i chi ddim bod gwilydd o’ch tas wair. Ond am yr hen betha newydd ’ma, John Jôs, welis i ’rioed ddioni ohonyn nhw. Dyna i chi Dic Jôs, Ty’n Mynydd ’rwan, mae o wedi gwario punna ar ryw hen gêr fel yna, ac mi fedra’r gwynt gario’i wair o bob ha. A ’dydw i ’n gweld fawr o gamp {68} ar laeth i gwarthaig nhw. Mi roth Elin Huws y Fron panad o dê i mi’r diwrnod o’r blaen, efo llefrith Bryn y Fawnog, a wir mi ’roedd o cin lasad ag y medrach chi welad gwaelod y jwg drwyddo fo. Ond fel na, John Jôs, fel ’roedd Elin Huws yn deyd, mi fedar rhai pobol werthu bleinion a’i alw fo’n dical.”

Er gwaethaf pob rhyw siarad fel hyn, dal i dyfu’r oedd gwair Bryn y Fawnog, ac nid oedd fawr o wahaniaeth yn y das wair ar ddiwedd yr haf; ni ddaeth esgyrn y gwartheg allan drwy’u crwyn, ac yr oedd Elin Huws yn dal i brynu llefrith yno. Wedyn dyna ardd Bodlondeb. Gallai drin tipyn ar honno i basio’r amser. Ond beth oedd rhyw ddegllath yswâr o ardd i ddyn oedd wedi arfer palu rhydau bwy’i gilydd. Nid oes lle mewn gardd i ddyn felly droi ei draed, a byddai wedi palu rhes cyn bod eisiau poeri ar ei law unwaith arno. Peth go anodd yw i ddyn ei gyfyngu ei hun i ardd wedi arfer efo chae. Lle i ddyn ysgwyd ei draed a’i ddwylo y galwai Wiliam Gruffydd gae. Galwai Williams y gweinidog beth felly’n eangder. “Eangder,” ebe’r olaf, gan chwifio’i ddwylo i’r dwyrain ac i’r gorllewin. “Cae,” ebe Wiliam Gruffydd, gan luchio tywarchen efo’i raw i’r awyr. Ond am ardd Bodlondeb, yr {69} oedd mwy na hynny o dir yn tyfu’n chwyn ym Mryn y Fawnog.

Yr oedd Wiliam Gruffydd yn ddarllenwr mawr, a meddyliodd wrth adael y chwarel y cai ddigon o amser i ddarllen. Ond ni waeth i chwi heb ddarllen oni chewch rywun i sgwrsio ag ef ynghylch a ddarllenasoch. Buan iawn y canfu’r hen frawd mai’r hyn a roddai flas iddo ar ddarllen oedd cael sgwrsio a dadleu efo Thwm y Ffridd yn y chwarel drannoeth. Bid sicr cai ddadleu yn yr Ysgol Sul hyd yn oed yn awr. Ond yr oedd aelodau’r dosbarth yn hŷn nag ef. Pobl cymdogaeth y pedwar ugain yma oeddynt, ac y mae tuedd mewn pobl o’r oed yna i grwydro. Dyna Wiliam Huws, Tan yr Ogo — byddai rhywbeth yn y wers yn siwr o fynd ag ef i Sir Fôn, sir ei enedigaeth a’r sir y’i magwyd. Ac o Sir Fôn y galwai’r gloch hwynt i’r ddaear ar ddiwedd yr Ysgol. Peth arall, nid oes cymaint amrywiaeth mewn dosbarth Ysgol Sul ag sydd mewn caban bwyta yn y chwarel. Yn y fan honno fe gewch esboniadau dynion na byddant byth yn darllen esboniadau.

Oedd, yr oedd ar Wiliam Gruffydd hiraeth am y chwarel. A’r bore hwn o haf, yr oedd rhywbeth yn yr awyr, yn arogl yr awyr, yn chwythiad yr awel, yn sŵn y {70} ffrwd, yn sŵn y chwarelwyr, ym mhopeth, yn debig i ryw fore, neu foreau flynyddoedd yn ôl pan âi yntau gyda’r dyrfa i fyny i Jerusalem ei fyd — y chwarel. Nid rhyfedd ei weled â’i bwysau ar y ddôr mor fore, oblegid cododd bob dydd byth am hanner awr wedi pump, chwarel neu beidio. Ac yn awr ni allai ddioddef gwawdiaith enw ei dŷ ddim rhagor. Bu yn bopeth ond Bodlondeb iddo ef. Yr oedd yn awr yn ugain munud i saith, a gwelai rai olaf y fintai yn mynd. Gwyddai mai hwy oedd yr olaf, oblegid yr oedd Robin Twm Sian yn eu mysg. Penderfynodd fynd i’r chwarel. Bu agos iddo beidio â mynd hyd drannoeth, gan ei bod mor hwyr, er y gellid cerdded yno’n hawdd mewn ugain munud. Arfer Wiliam Gruffydd oedd cychwyn i’r chwarel awr cyn caniad, a threulio’r amser oedd ganddo wrth gefn i orffwys yn y caban. Ond fe aeth dyhead yn drech na’i ysbryd prydlon y tro hwn. Rhedodd i’r tŷ a rhoddodd ei esgidiau hoelion mawr am ei draed, a ffwrdd ag ef ar ôl y chwarelwyr eraill. Yr oeddynt hwy yn colli ar ben yr Allt Goch pan gyrhaeddodd ef ei gwaelod. Wedi cyrraedd y chwarel aeth ar ei union i’w hen le yn y shed. Yno, yn ei le ef, ac yn eistedd ar ei blocyn, yr oedd Wil Wmffra, ei {71} hen bartner. Gwelwodd yr olaf pan welodd Wiliam Gruffydd, ac ni allai ddywedyd gair. Mewn gwirionedd, distawodd popeth yn y shed ond yr injian. Pe deuthai’r hen frawd o fyd arall, ni buasai yno fwy o syndod. Modd bynnag, dyma Jôs, y stiward bach, yno o rywle, ac fe arbedodd iddynt ddywedyd pethau chwithig.

“Hylo’r hen fachgian,” ebe Jôs, gan ddodi ei law ar ei ysgwydd. Dyn oedd y stiward bach a allai roddi ei law ar ysgwydd hen ddyn, ac ar ysgwydd plentyn, a dywedyd, “Hylo’r hen fachgian,” heb dramgwyddo’r un o’r ddau.

“Hylo’r hen fachgian,” ebe Jôs, “Sut ma’ i’n dwad? Wedi rhoi tro i edrach amdanom ni?”

“Wel ia, fachgen, wedi blino adra rwsud, ac yn meddwl y liciwn i ddwad yn f’ôl.”

“Da iawn, mi chwiliwn ni am rwbath go ysgafn i chi neud.”

“Wath gin i am i ysgafn o, ’rydw i ’n ddigon ’tebol.”

Pletiodd Jôs ei wefusau. Fe wyddai yn eithaf da fod digon o nerth yng ngewynnau Wiliam Gruffydd eto, i’w roi ymhen un o weithwyr caletaf y gwaith. Ond nid oedd yno le iddo mewn bargen.

{72} “Rhoswch am funud, Wiliam Gruffydd.” Ac aeth allan. Yr oedd yn ei ôl mewn eiliad.

“Mi gewch weithio yn lle Dafydd Wiliam Llwyn Drain heiddiw; mae o adra efo’i wair ddyliwn, ac mi gawn weld beth fydd yma at y fory i chi.”

Felly bu, ac i’r shed bach a Wiliam Gruffydd. Rhyw ddistaw iawn yr oedd pawb yn y fan honno wedyn, a theimlai nad oedd ar neb eisiau ei weld yn ôl. Ond chwarae teg i’w hen gyfeillion, wedi eu syfrdanu ormod i siarad yr oeddynt. Cafodd sgwrs efo Wil Bach yr Efail pan oedd hwnnw’n myned trwy’r shed heb neges neilltuol, a theimlodd am funud fod yr hen amser wedi dyfod yn ôl.

Daeth awr ginio, a rhuthrodd pawb am y cyntaf i’r caban. Dyna’r munud cyntaf i Wiliam Gruffydd gofio na ddaeth a’i ginio gydag ef. Ond gwnaed y diffyg hwnnw i fyny ar unwaith. Cafodd ddigon o frechdan gan un, caws gan un arall, a rhoddodd Wil Wmffra ddigon o dê iddo a chaead ei biser i’w yfed. Eithr ni chafodd eistedd yn ei hen le ymhen draw’r caban yn wynebu pawb. Llenwid hwnnw’n awr gan Morgan Owan — dyn oedd yn ddadleuydd mawr yn ei olwg ei hun ac yng ngolwg dynion bach eu hamgyffred {73} a’u darllen. Suddodd calon Wiliam Gruffydd wrth weled hwnnw’n ei le ef. Un o’r dynion hynny a siaradai yn—<i>yddol</i> ac mewn—<i>olrwydd</i> oedd Morgan Owan, ac i rai, niwl geiriau mawr oedd niwl dynion mawr.

Nid oedd gan yr hen ŵr lawer o le i eistedd chwaith. Teimlodd hynny gyntaf pan roddodd rhywun hergwd i’w benelin, nes colli ohono’i dê hyd ei ddillad. Gwesgid Wil Bach yntau yn y gongl wrth y drws, a bu agos iddo weiddi, “Closiwch i fyny, lads”; pan gofiodd, brathodd ei dafod. Modd bynnag, i Wiliam Gruffydd, yr oedd un yn ormod ar y fainc honno.

“Beth wyt ti’n feddwl o spitsh y Mawr,” ebe Dafydd Rolant heb gyfarch neb yn neilltuol.

“Tydw i ’n meddwl dim ohoni,” ebe Wiliam Gruffydd.

“Sut felly?” ebe Morgan Owan.

“Wel, ’does dim yn dangos yn well na’r spitsh yna i fod o wedi troi i gefn ar y werin.”

(“Gwrando di ar yr hen Wil Gruff,” ebe Wil Bach gydag edmygedd, yng nghlust Dafydd Rolant).

“Sut yr ydach chi’n medru deyd hynny?” ebe Morgan Owan gyda phwyslais ar bob llythyren.

{74} “Wel, mi fasa’n rhyfadd iawn gin i feddwl ma’r dyn fu’n siarad am hawlia’r gweithiwr gyda’r fath hwyl ers talwm fasa’n gweiddi am hel i plant nhw i’r Rhyfal,” ebe Wiliam Gruffydd.

“Tydi hwnna ddim ar y pwynt,” ebe Dafydd Rolant. Tydi’r ffaith fod dyn am gael terfyn ar y Rhyfal ddim yn deyd i fod o wedi troi ’i gefn ar y gweithiwr.”

“Clywch, clywch,” ebe llais.

“Ydi,” ebe Wiliam Gruffydd, “tasa gyno fo rywfaint o deimlad at griadur tlawd, mi fasa’n cymyd dipyn bach mwy ara deg; a pheth arall, mi allsa fod wedi bod yn fwy ara deg ar y cychwyn. Achos plant y gweithiwrs ceith hi gleta ymhob Rhyfal.”

“Wel,” ebe Morgan Owan, ar ôl pesychiad pwysig, “rhaid inni edrach ar gyffredinolrwydd pethau. Wedi’r cyfan, rhyw swm bach negyddol ydan ni yng nghyfundrefn wleidyddol y byd. A siarad yn athronyddol, ma’n rhaid i bethau fod fel hyn. Mae cwmpawd y byd gwleidyddol yn troi o gwmpas canolbwynt na wyddom ni ddim byd amdano, a ’rwan, y peth gora inni i gyd ydi dwad a hyn i’w ddiweddbwynt, a hynny yn sydyn.”

“Clywch, clywch,” ebe addolwyr y niwl.

{75} Gwelodd Wiliam Gruffydd mai ffôl oedd ateb dyn cyn lleied ei ddychymig, a bu’n ddistaw o hynny i’r diwedd.

Gweithiodd y prynhawn drachefn a cherddodd adref gyda’i hen gyfeillion. Ymadawai â hwy wrth groesffordd y siop a cherddai’r gweddill o’r ffordd ei hunan. Yr oedd yn rhaid iddo fyned heibio i Fryn y Fawnog. Heb yn wybod iddo’i hun, trodd drwy’r llidiart. Dwy gongl gron a droai o’r ffordd at lidiart Bryn y Fawnog, ac mae’n haws i ysgwydd dyn droi heibio i gongl gron na heibio i un ysgwâr. Ac felly y’i cafodd ei hun yn agor drws y tŷ ac yn myned i mewn.

“Tada,” ebe hogyn bach pedair oed dan redeg i’w gyfarfod. Ond trodd yn ei ôl pan welodd mai Wiliam Gruffydd oedd yno ac nid ei dad. Cododd y fam ei golygon mewn syndod oddiar ei babi wrth ei weled.

“Sut ydach chi heno, Wiliam Gruffydd,” ebe hi yn ffrwcslyd.

Ond yr oedd Wiliam Gruffydd eisoes ar ei ffordd at y drws yn troi ei gefn ar nefoedd a welodd yntau unwaith.

Edrychai ddeng mlynedd yn hŷn pan gerddai at Fodlondeb, ac yr oedd ei lygaid ymhellach nag erioed yn ei ben.

{76} “Celwydd,” ebe fe wrtho’i hun, “celwydd bob gair ydi deyd mewn cwarfodydd coffa, a chwarfodydd ymadawol, bod bwlch mawr ar ôl dynion yn y byd yma. Tasa nhw’n fylcha mawr, fasa nhw ddim mor hawdd i cau. Fedar neb ail agor adwyon wedi i cau unwath ym myd pobl, beth bynnag.”

Aeth adref, taflodd ei esgidiau hoelion mawr i’r gegin bach. Ni wisgodd hwynt drachefn — nac unrhyw esgidiau eraill chwaith.

<date>Hydref, 1922.</date>
</xml>
